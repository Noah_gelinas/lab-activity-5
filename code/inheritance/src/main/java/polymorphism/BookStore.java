package polymorphism;

public class BookStore {
    public static void main(String... args) {
        Book[] books = new Book[5];
        books[0]=new Book("Charles Dickens", "A Christmas Carol");
        books[1]=new ElectronicBook("Fyodor Dostoevsky", "The Idiot",500);
        books[2]=new Book("Stephen King", "It");
        books[3]=new ElectronicBook("Leo Tolstoy", "War and Peace",450);
        books[4]=new ElectronicBook("Ernest HemingWay", "A Farewell to Arms",600);

        for (int i=0;i<books.length;i++) {
            System.out.println("\n"+books[i]);
        } 
    }    
}
