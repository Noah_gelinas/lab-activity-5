package polymorphism;

public class ElectronicBook extends Book{
    private int numberBytes;

    public int getNumberBytes() {
        return numberBytes;
    }

    public ElectronicBook(String author,String title,int numberBytes){
        super(author,title);
        this.numberBytes=numberBytes;
    }

    public String toString() {
        String fromBase=super.toString();
        fromBase=fromBase+", File size: " +numberBytes;
        return fromBase;
    }
}
