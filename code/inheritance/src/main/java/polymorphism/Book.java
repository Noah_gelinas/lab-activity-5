package polymorphism;

public class Book {
    protected String title;
    private String author;

    public String getTitle() {
        return this.title;
    }
    public String getAuthor(){
        return this.author;
    }

    public Book(String author,String title) {
        this.author=author;
        this.title=title;
    }

    public String toString(){
    return "Author: " +this.author+ ", title: "+this.title;
    }
}
